Rails.application.routes.draw do



  resources :unit_groups
  resources :client_applications
  resources :discount_plans
  resources :discount_plan_controls
  resources :channel_rates
  resources :discount_plan_discounts
  resources :unit_types
  resources :invoiceable_fees
  resources :units
  resources :unit_amenities	

  get 'welcome/index'
  get '/upload' => 'welcome#upload', :as => :upload
  post '/upload' => 'welcome#upload_post', :as => :upload_post

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
end
