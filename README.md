# etl-exercise

ETL Exercise server developed by Eduardo Rodriguez. It uses Postgresql as its db and for the ETL, it has a custom gem called "etl-gem"; this gem is located under "gems". The job of this server is to upload the data of a json file to the db. On the root page of the server, one can find the models that will be filled by the different jsons. On the top of the navigation bar there is a section called "Upload Json", this is where one can upload a JSON file and the server will take care of the rest. It uses bootstrap to make the server more appealing and less ugly too see.

Developer Issues:
-The relations are inverse.
-Couldn't fully use the LightService Gem, when wanting to do a class that is both an action and an organizer, the code only executed the first sub-action, halting the rest of the flow. Resorted to make 3 big actions (Extract, Transform and Load).

P.S

I had fun with this exercise, even though I lacked many areas, for example I believe the Load section has a way more better and elegant way to be implemented, applying the correct design patterns. 