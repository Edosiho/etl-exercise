require 'test_helper'

class InvoiceableFeesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get invoiceable_fees_new_url
    assert_response :success
  end

  test "should get create" do
    get invoiceable_fees_create_url
    assert_response :success
  end

  test "should get update" do
    get invoiceable_fees_update_url
    assert_response :success
  end

  test "should get edit" do
    get invoiceable_fees_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get invoiceable_fees_destroy_url
    assert_response :success
  end

  test "should get show" do
    get invoiceable_fees_show_url
    assert_response :success
  end

  test "should get index" do
    get invoiceable_fees_index_url
    assert_response :success
  end

end
