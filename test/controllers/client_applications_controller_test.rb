require 'test_helper'

class ClientApplicationsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get client_applications_new_url
    assert_response :success
  end

  test "should get create" do
    get client_applications_create_url
    assert_response :success
  end

  test "should get update" do
    get client_applications_update_url
    assert_response :success
  end

  test "should get edit" do
    get client_applications_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get client_applications_destroy_url
    assert_response :success
  end

  test "should get show" do
    get client_applications_show_url
    assert_response :success
  end

  test "should get index" do
    get client_applications_index_url
    assert_response :success
  end

end
