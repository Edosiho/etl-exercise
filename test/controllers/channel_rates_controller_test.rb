require 'test_helper'

class ChannelRatesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get channel_rates_index_url
    assert_response :success
  end

  test "should get show" do
    get channel_rates_show_url
    assert_response :success
  end

  test "should get new" do
    get channel_rates_new_url
    assert_response :success
  end

  test "should get edit" do
    get channel_rates_edit_url
    assert_response :success
  end

end
