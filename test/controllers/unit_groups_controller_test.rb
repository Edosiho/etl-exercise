require 'test_helper'

class UnitGroupsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get unit_groups_new_url
    assert_response :success
  end

  test "should get create" do
    get unit_groups_create_url
    assert_response :success
  end

  test "should get update" do
    get unit_groups_update_url
    assert_response :success
  end

  test "should get edit" do
    get unit_groups_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get unit_groups_destroy_url
    assert_response :success
  end

  test "should get show" do
    get unit_groups_show_url
    assert_response :success
  end

  test "should get index" do
    get unit_groups_index_url
    assert_response :success
  end

end
