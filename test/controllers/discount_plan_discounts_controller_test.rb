require 'test_helper'

class DiscountPlanDiscountsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get discount_plan_discounts_new_url
    assert_response :success
  end

  test "should get create" do
    get discount_plan_discounts_create_url
    assert_response :success
  end

  test "should get update" do
    get discount_plan_discounts_update_url
    assert_response :success
  end

  test "should get edit" do
    get discount_plan_discounts_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get discount_plan_discounts_destroy_url
    assert_response :success
  end

  test "should get show" do
    get discount_plan_discounts_show_url
    assert_response :success
  end

  test "should get index" do
    get discount_plan_discounts_index_url
    assert_response :success
  end

end
