require 'test_helper'

class DiscountPlanControlsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get discount_plan_controls_new_url
    assert_response :success
  end

  test "should get create" do
    get discount_plan_controls_create_url
    assert_response :success
  end

  test "should get update" do
    get discount_plan_controls_update_url
    assert_response :success
  end

  test "should get edit" do
    get discount_plan_controls_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get discount_plan_controls_destroy_url
    assert_response :success
  end

  test "should get show" do
    get discount_plan_controls_show_url
    assert_response :success
  end

  test "should get index" do
    get discount_plan_controls_index_url
    assert_response :success
  end

end
