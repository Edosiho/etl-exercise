class AddDiscountPlanIdToClientApplication < ActiveRecord::Migration[5.2]
  def change
    add_column :client_applications, :discount_plan_id, :string
  end
end
