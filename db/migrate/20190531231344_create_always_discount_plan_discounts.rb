class CreateAlwaysDiscountPlanDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :always_discount_plan_discounts, :id => false do |t|
      t.string :id

      t.timestamps
    end
  end
end
