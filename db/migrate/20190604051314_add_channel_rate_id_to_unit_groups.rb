class AddChannelRateIdToUnitGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_groups, :channel_rate_id, :string
  end
end
