class AddUnitIdToUnitAmenities < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_amenities, :unit_id, :string
  end
end
