class AddDiscountPlanIdToTenantAccountKinds < ActiveRecord::Migration[5.2]
  def change
    add_column :tenant_account_kinds, :discount_plan_id, :string
  end
end
