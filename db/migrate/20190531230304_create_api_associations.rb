class CreateApiAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :api_associations,:id => false do |t|
      t.string :id

      t.timestamps
    end
  end
end
