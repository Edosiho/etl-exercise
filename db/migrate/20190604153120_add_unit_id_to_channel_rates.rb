class AddUnitIdToChannelRates < ActiveRecord::Migration[5.2]
  def change
    add_column :channel_rates, :unit_id, :string
  end
end
