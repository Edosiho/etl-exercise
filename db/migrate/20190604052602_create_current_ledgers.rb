class CreateCurrentLedgers < ActiveRecord::Migration[5.2]
  def change
    create_table :current_ledgers,:id => false do |t|
      t.string :id

      t.timestamps
    end
  end
end
