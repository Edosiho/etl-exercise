class AddUnitIdToCurrentLedger < ActiveRecord::Migration[5.2]
  def change
    add_column :current_ledgers, :unit_id, :string
  end
end
