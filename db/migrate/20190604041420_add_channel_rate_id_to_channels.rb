class AddChannelRateIdToChannels < ActiveRecord::Migration[5.2]
  def change
    add_column :channels, :channel_rate_id, :string
  end
end
