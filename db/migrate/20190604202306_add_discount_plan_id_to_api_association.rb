class AddDiscountPlanIdToApiAssociation < ActiveRecord::Migration[5.2]
  def change
    add_column :api_associations, :discount_plan_id, :string
  end
end
