class AddUnitGroupIdToInvoiceableFees < ActiveRecord::Migration[5.2]
  def change
    add_column :invoiceable_fees, :unit_group_id, :string
  end
end
