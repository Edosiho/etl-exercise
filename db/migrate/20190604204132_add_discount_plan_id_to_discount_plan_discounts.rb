class AddDiscountPlanIdToDiscountPlanDiscounts < ActiveRecord::Migration[5.2]
  def change
    add_column :always_discount_plan_discounts, :discount_plan_id, :string
  end
end
