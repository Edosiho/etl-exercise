class AddDiscountPlanControlIdToDiscountPlansConnect < ActiveRecord::Migration[5.2]
  def change
    add_column :discount_plans_connects, :discount_plan_id, :string
  end
end
