class RemoveCurrentLedgerIdFromUnits < ActiveRecord::Migration[5.2]
  def change
    remove_column :units, :current_ledger_id, :string
  end
end
