class AddUnitGroupIdToUnitGroupChannelRate < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_group_channel_rates, :unit_group_id, :string
  end
end
