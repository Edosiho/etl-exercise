class AddUnitGroupIdToUnitTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_types, :unit_group_id, :string
  end
end
