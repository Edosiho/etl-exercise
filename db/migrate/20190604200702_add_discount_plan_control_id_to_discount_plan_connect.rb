class AddDiscountPlanControlIdToDiscountPlanConnect < ActiveRecord::Migration[5.2]
  def change
    add_column :discount_plans_connects, :discount_plan_control_id, :string
  end
end
