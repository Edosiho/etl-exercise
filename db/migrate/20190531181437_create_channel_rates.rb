class CreateChannelRates < ActiveRecord::Migration[5.2]
  def change
    create_table :channel_rates,:id => false do |t|
      t.string :id
      t.string :base_rate_type
      t.string :modifier_type
      t.boolean :turned_on
      t.boolean :turned_off_on
      t.float :rate
      t.float :amount
      t.string :channel_name

      t.timestamps
    end
  end
end
