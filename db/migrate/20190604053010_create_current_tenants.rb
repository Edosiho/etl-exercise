class CreateCurrentTenants < ActiveRecord::Migration[5.2]
  def change
    create_table :current_tenants,:id => false do |t|
      t.string :id

      t.timestamps
    end
  end
end
