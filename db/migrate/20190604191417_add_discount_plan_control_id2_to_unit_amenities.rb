class AddDiscountPlanControlId2ToUnitAmenities < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_amenities, :discount_plan_control_id, :string
  end
end
