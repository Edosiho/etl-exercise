class AddChannelIdToChannelRates < ActiveRecord::Migration[5.2]
  def change
    add_column :channel_rates, :channel_id, :string
  end
end
