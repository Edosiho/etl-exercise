class AddUnitIdToUnitGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_groups, :unit_id, :string
  end
end
