class AddChannelRateIdToUnitGroupChannelRate < ActiveRecord::Migration[5.2]
  def change
    add_column :unit_group_channel_rates, :channel_rate_id, :string
  end
end
