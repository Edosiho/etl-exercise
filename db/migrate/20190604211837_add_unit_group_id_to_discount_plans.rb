class AddUnitGroupIdToDiscountPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :discount_plans, :unit_group_id, :string
  end
end
