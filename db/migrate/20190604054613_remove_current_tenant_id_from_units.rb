class RemoveCurrentTenantIdFromUnits < ActiveRecord::Migration[5.2]
  def change
    remove_column :units, :current_tenant_id, :string
  end
end
