class CreateTenantAccountKinds < ActiveRecord::Migration[5.2]
  def change
    create_table :tenant_account_kinds,:id => false do |t|
      t.string :id

      t.timestamps
    end
  end
end
