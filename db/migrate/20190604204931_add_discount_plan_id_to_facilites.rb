class AddDiscountPlanIdToFacilites < ActiveRecord::Migration[5.2]
  def change
    add_column :facilities, :discount_plan_id, :string
  end
end
