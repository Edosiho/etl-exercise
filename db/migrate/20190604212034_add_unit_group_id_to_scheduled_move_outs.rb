class AddUnitGroupIdToScheduledMoveOuts < ActiveRecord::Migration[5.2]
  def change
    add_column :scheduled_move_outs, :unit_group_id, :string
  end
end
