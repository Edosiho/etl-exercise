class AddDiscountPlanIdToDiscountTypeDiscount < ActiveRecord::Migration[5.2]
  def change
    add_column :discount_plan_discounts, :discount_plan_id, :string
  end
end
