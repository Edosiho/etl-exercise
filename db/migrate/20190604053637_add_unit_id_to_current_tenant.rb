class AddUnitIdToCurrentTenant < ActiveRecord::Migration[5.2]
  def change
    add_column :current_tenants, :unit_id, :string
  end
end
