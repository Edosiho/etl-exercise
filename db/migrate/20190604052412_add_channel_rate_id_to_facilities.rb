class AddChannelRateIdToFacilities < ActiveRecord::Migration[5.2]
  def change
    add_column :facilities, :channel_rate_id, :string
  end
end
