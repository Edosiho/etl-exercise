class CreateScheduledMoveOuts < ActiveRecord::Migration[5.2]
  def change
    create_table :scheduled_move_outs,:id => false do |t|
      t.string :id

      t.timestamps
    end
  end
end
