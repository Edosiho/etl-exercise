# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
ChannelRate.create("id"=>"bd13de83-afe9-460a-982b-7642ea2cf893",
 "base_rate_type"=>"standard_rate",
 "modifier_type"=>"fixed",
 "turned_on"=>true,
 "turned_off_on"=>nil,
 "rate"=>27.5,
 "amount"=>27.5,
 "channel_name"=>"Web")