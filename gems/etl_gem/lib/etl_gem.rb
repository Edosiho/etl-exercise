	require "json"
	require "light-service"

	class Etl  
		def self.extract(jsonfile)
			file = File.read(jsonfile)
			file = JSON.parse(file)
			file.reject! {|k| k == "meta"}
			return file
		end	

		def self.transform(file)
			@structured = {}
			self.parseHash(file)
			self.deleteDup(@structured)
			self.trimHash(@structured)
			return @structured
		end	

		def self.dumpV(val,arr)
			if(val.is_a?(Array))
				addToArray(val,arr)
			elsif(val.is_a?(Hash))
				arr.push(cleanHashes(val))
				parseHash(val)
			end
			return arr
		end

		def self.parseHash(hashp)
			hashp.each do |k,v|
				if(v.is_a?(Array) or v.is_a?(Hash))
					if(@structured[k].nil?)
						arr = []
						@structured[k] = dumpV(v,arr)
					else
						@structured[k] = dumpV(v,@structured[k])
					end
				end
			end
		end

		def self.addToArray(val,arr)
			val.each do |data|
				if(!data.is_a?(String))
					hsh = {}
					data.each do |k,v|
							if(!v.is_a?(Array) and !v.is_a?(Hash))
								hsh[k] =v
							else
								if(v.is_a?(Hash))
									hsh[k] = v["id"]
								elsif(v.is_a?(Array))
									iarr = []
									v.each do |inner|
										if(!inner["id"].nil?)
											iarr.push(inner["id"])
										else
											iarr.push(inner)
										end
									end
									hsh[k] = iarr
								end
									if(@structured[k].nil?)
										@structured[k] = dumpV(v,[])
									else
										@structured[k] = dumpV(v,@structured[k])
									end
							end
					end
							arr.push(hsh)
					end
			end
		end

	

		def self.cleanHashes(hsh)
			new_hsh = {}
			hsh.each do |k,v|
				if(!v.is_a?(Hash))
					new_hsh[k] = v
				end
			end
			return new_hsh
		end



		def self.deleteDup(hsh)
			hsh.each do |k, v|
				arr = []
				indx = []
				for i in 0..v.length-1 do
					tmp =  v[i]["id"]
					if(!arr.include?(tmp))
						arr.push(tmp)
					else
						indx.push(i)
					end
				end
				indx = indx.sort_by { |number| -number }
				deleteIndx(v,indx)
			end
		end

		def self.deleteIndx(arr,indx)
			for i in 0..indx.size-1 do
				arr.delete_at(indx[i])
			end
		end

		def self.trimHash(hsh)
			hsh.each do |k,v|
				if(v.size == 0)
					hsh.reject! {|val| val == k}
				end
			end
		end

		def self.newInstance(type,values)
			puts "######"
			puts type
			puts values
			puts "######"
			case type
			when 'channel_rate', 'channel_rate_id', 'channel_rate_ids'
				ChannelRate.new(values)
			when 'unit_amenities', 'unit_amemity_ids'
				UnitAmenity.new(values)
			when 'units'
				Unit.new(values)
			when 'invoiceable_fees'
				InvoiceableFee.new(values)
			when 'unit_type', 'unit_type_id'
				UnitType.new(values)
			when 'discount_plan_discounts'
				DiscountPlanDiscount.new(values)
			when 'discount_plan_controls'
				DiscountPlanControl.new(values)
			when 'client_applications'
				ClientApplication.new(values)
			when 'discount_plans', 'discount_plan_ids'
				DiscountPlan.new(values)
			when 'unit_groups', 'unit_group_id'
				UnitGroup.new(values)
			when 'facility', 'facility_id','facility_ids'
				Facility.new(values)
			when 'channel', 'channel_id'
				Channel.new(values)
			when 'scheduled_move_out', 'scheduled_move_out_ids'
				ScheduledMoveOut.new(values)
			when 'tenant_account_kind', 'tenant_account_kind_id'
				TenantAccountKind.new(values)
			when 'current_tenant', 'current_tenant_id'
				CurrentTenant.new(values)
			when 'current_ledger', 'current_ledger_id'
				CurrentLedger.new(values)
			when 'api_association', 'api_association_ids'
				ApiAssociation.new(values)
			when 'always_discount_plan_discount_id', 'always_discount_plan_discount'
				AlwaysDiscountPlanDiscount.new(values)

			else
				puts type
				raise "table is not on DB instance"
			end
		end


		#asssoc es el id.

		def self.searchObj(clss,id)
			puts clss
			puts id
			obj = nil
			case clss
			when 'channel_rate', 'channel_rate_id', 'channel_rate_ids'
				obj =ChannelRate.find_by(id: id)
			when 'unit_amenities', 'unit_amemity_ids'
				obj =UnitAmenity.find_by(id: id)
			when 'units'
				obj =Unit.find_by(id: id)
			when 'invoiceable_fees'
				obj =InvoiceableFee.find_by(id: id)
			when 'unit_type', 'unit_type_id'
				obj =UnitType.find_by(id: id)
			when 'discount_plan_discounts'
				obj =DiscountPlanDiscount.find_by(id: id)
			when 'discount_plan_controls'
				obj =DiscountPlanControl.find_by(id: id)
			when 'client_applications'
				obj =ClientApplication.find_by(id: id)
			when 'discount_plans', 'discount_plan_ids'
				obj =DiscountPlan.find_by(id: id)
			when 'unit_groups', 'unit_group_id'
				obj =UnitGroup.find_by(id: id)
			when 'facility', 'facility_id','facility_ids'
				obj =Facility.find_by(id: id)
			when 'channel', 'channel_id'
				obj =Channel.find_by(id: id)
			when 'scheduled_move_out', 'scheduled_move_out_ids'
				obj =ScheduledMoveOut.find_by(id: id)
			when 'tenant_account_kind', 'tenant_account_kind_id'
				obj =TenantAccountKind.find_by(id: id)
			when 'api_association', 'api_association_ids'
				obj =ApiAssociation.find_by(id: id)
			when 'current_tenant', 'current_tenant_id'
				obj = CurrentTenant.find_by(id: id)
			when 'current_ledger', 'current_ledger_id'
				obj = CurrentLedger.find_by(id: id)
			when 'always_discount_plan_discount_id', 'always_discount_plan_discount'
				obj = AlwaysDiscountPlanDiscount.find_by(id: id)
			else
				puts clss
			end

			return obj
		end

		def self.getObject(clss,id)
			puts "getObject"
			obj = searchObj(clss,id)
			if(obj == nil)
				puts "in getobject"
				obj = newInstance(clss,{"id"=>id})
			end
			#falta asignar el obj.yeet >> yeet
			return obj

		end
		def self.addtoClass(obj,assoc,relatedobj)
			puts "entering addToclass"
			#gets the obj, or creates it
			if(!assoc.nil?)
				newrel = getObject(relatedobj,assoc)

				 newrel.save
				 puts "#{obj.attributes} base object"
				 puts "#{relatedobj} associated object"
				#assings it
				case relatedobj
				when 'channel_rate' , 'channel_rate_id', 'channel_rate_ids'
					obj.channel_rate << newrel
				when 'unit_amenities', 'unit_amemity_ids'
					obj.unit_amenities << newrel
				when 'units'
					obj.unit << newrel
				when 'invoiceable_fees'
					obj.invoiceable_fee << newrel
				when 'unit_type', 'unit_type_id'
					obj.unit_type << newrel
				when 'discount_plan_discounts'
					obj.discount_plan_discount << newrel
				when 'discount_plan_controls'
					obj.discount_plan_control<< newrel
				when 'client_applications'
					obj.client_application << newrel
				when 'discount_plans', 'discount_plan_ids'
					puts "ENTERING DISCOUNT PLANS"
					puts newrel
					obj.discount_plan << newrel
				when 'unit_groups', 'unit_group_id'
					obj.unit_group << newrel
				when 'facility', 'facility_id','facility_ids'
					obj.facility << newrel
				when 'channel', 'channel_id'
					obj.channel << newrel
				when 'scheduled_move_out', 'scheduled_move_out_ids'
					obj.scheduled_move_out << newrel
				when 'tenant_account_kind','tenant_account_kind_id'
					obj.tenant_account_kind << newrel
				when 'api_association', 'api_association_ids'
					obj.api_association << newrel
				when 'current_tenant', 'current_tenant_id'
					obj.current_tenant << newrel
				when 'current_ledger', 'current_ledger_id'
					obj.current_ledger << newrel
				when 'always_discount_plan_discount_id', 'always_discount_plan_discount'
					obj.always_discount_plan_discount << newrel
				else
					puts relatedobj
					raise "table is not on DB addtoclass"
				end
			end

		end

		def self.addRelations(object,rels)
			puts "entering addRelations"
			rels.each do |k,v|
				puts v
				if(v.is_a?(Array))
					v.each do |assoc|
						addtoClass(object,assoc,k)
					end
				else
					addtoClass(object,v,k)
				end
			end
		end

		def self.spliceValues(hsh,singles,plural)
			regulars = {}
			classes = {}
			hsh.each do |k,v|
				if ( ( (k.split("_").last()=="id" or (k.split("_").last()=="ids") ) and k!= "id") or (singles.include?(k) or plural.include?(k)))
					classes[k] =v
				else
					regulars[k] = v
				end
			end
		return regulars,classes
		end


		def self.uploadtodb(iclass,values,singles, plurals)
			split = spliceValues(values,singles,plurals)
			object = objUpload(iclass,split[0])
			object.save!
			relations = addRelations(object,split[1])

		end

		def self.load(hsh,singles,plurals)
			hsh.each do |k,v|
				v.each do |instance|
					uploadtodb(k,instance,singles,plurals)
				end
			end
		end	

		def self.objUpload(iclass,vals)
			obj = searchObj(iclass,vals["id"])
			if (!obj.nil?)
				obj.update_attributes(vals)
			else
				obj = newInstance(iclass,vals)
			end
			return obj
		end

end





class EtlService extend LightService::Organizer  
	def self.call( file)
		with(:file =>file).reduce(
			ExtractsAction,
			TransformsAction,
			LoadsAction
		)
	end
end

class ExtractsAction extend LightService::Action
	expects :file
	promises :hsh_file
	executed do |ctx|
		ctx.hsh_file = preprocess(File.read(ctx.file))

	end
	def self.preprocess(file)
		file = JSON.parse(file)
		file.reject! {|k| k == "meta"}
		return file
	end
end

class TransformsAction extend LightService::Action
	expects :hsh_file
	promises :parsed_hsh

	executed do |ctx|
		ctx.parsed_hsh = Etl.transform(ctx.hsh_file)
	end
end

class LoadsAction extend LightService::Action

	expects :parsed_hsh
	
	executed do |ctx|
		arrs = getNames()
		Etl.load(ctx.parsed_hsh,arrs[0],arrs[1])
	end

	def self.getNames()
		plurals = []
		singles = []

		ActiveRecord::Base.connection.tables.map do |model|
			singles.push( model.singularize)
			plurals.push(model)
		end
		return singles, plurals
	end

end


=begin
class LoadsAction
 extend LightService::Action
 extend LightService::Organizer
	expects :parsed_hsh

	executed do |ctx|
		arrs = LoadHelper.getNames()
		ctx.parsed_hsh.each do |k,v|
			v.each do |instance|
				with(k: k,instance: instance, singles: arrs[0], plurals: arrs[1] ).reduce(
					SpliceValues,
					UploadObj,
					SaveObj,
					AddRelations
				)
			end
		end
	end
end

class SpliceValues extend LightService::Action
	expects :instance
	expects :k
	promises :split

	regulars = {}
	classes = {}
	executed do |ctx|
		ctx.instance.each do |kk,vv|
			if ( ( (kk.split("_").last()=="id" or (kk.split("_").last()=="ids") ) and kk!= "id") or (ctx.singles.include?(kk) or ctx.plural.include?(kk)))
				classes[kk] =vv
			else
				regulars[kk] = vv
			end
		end
		ctx.split = []
		ctx.split.push(regulars)
		ctx.split.push(classes)
		puts "yeet"

	end
end

class UploadObj extend LightService::Action
	promises :obj
	executed do |ctx|
		ctx.obj = LoadHelper.searchObj(ctx.k,ctx.split[0]["id"])
		if (!ctx.obj.nil?)
			ctx.obj.update_attributes(ctx.split[0])
		else
			ctx.obj = LoadHelper.newInstance(ctx.k,ctx.split[0])
		end
	end
end

class SaveObj extend LightService::Action
	executed do |ctx|
		ctx.obj.save!
	end
end

 class AddRelations extend LightService::Action
	executed do |ctx|
		puts "entering addRelations"
		ctx.splig[1].each do |kk,vv|
			puts vv
			if(vv.is_a?(Array))
				vv.each do |assoc|
					LoadHelper.addtoClass(ctx.obj,assoc,kk)
				end
			else
				LoadHelper.addtoClass(object,vv,kk)
			end
		end
	end
end


class LoadHelper

	def self.getObject(clss,id)
			puts "getObject"
			obj = searchObj(clss,id)
			if(obj == nil)
				puts "in getobject"
				obj = newInstance(clss,{"id"=>id})
			end
			#falta asignar el obj.yeet >> yeet
			return obj
		end


	def self.addtoClass(obj,assoc,relatedobj)
		puts "entering addToclass"
		#gets the obj, or creates it
		if(!assoc.nil?)
			newrel = getObject(relatedobj,assoc)

			 newrel.save
			 puts "#{obj.attributes} base object"
			 puts "#{relatedobj} associated object"
			#assings it
			case relatedobj
			when 'channel_rate' , 'channel_rate_id', 'channel_rate_ids'
				obj.channel_rate << newrel
			when 'unit_amenities', 'unit_amemity_ids'
				obj.unit_amenities << newrel
			when 'units'
				obj.unit << newrel
			when 'invoiceable_fees'
				obj.invoiceable_fee << newrel
			when 'unit_type', 'unit_type_id'
				obj.unit_type << newrel
			when 'discount_plan_discounts'
				obj.discount_plan_discount << newrel
			when 'discount_plan_controls'
				obj.discount_plan_control<< newrel
			when 'client_applications'
				obj.client_application << newrel
			when 'discount_plans', 'discount_plan_ids'
				obj.discount_plan << newrel
			when 'unit_groups', 'unit_group_id'
				obj.unit_group << newrel
			when 'facility', 'facility_id','facility_ids'
				obj.facility << newrel
			when 'channel', 'channel_id'
				obj.channel << newrel
			when 'scheduled_move_out', 'scheduled_move_out_ids'
				obj.scheduled_move_out << newrel
			when 'tenant_account_kind','tenant_account_kind_id'
				obj.tenant_account_kind << newrel
			when 'api_association', 'api_association_ids'
				obj.api_association << newrel
			when 'current_tenant', 'current_tenant_id'
				obj.current_tenant << newrel
			when 'current_ledger', 'current_ledger_id'
				obj.current_ledger << newrel
			when 'always_discount_plan_discount_id', 'always_discount_plan_discount'
				obj.always_discount_plan_discount << newrel
			else
				puts relatedobj
				raise "table is not on DB addtoclass"
			end
		end
	end

	def self.searchObj(clss,id)
		puts clss
		puts id
		obj = nil
		case clss
		when 'channel_rate', 'channel_rate_id', 'channel_rate_ids'
			obj =ChannelRate.find_by(id: id)
		when 'unit_amenities', 'unit_amemity_ids'
			obj =UnitAmenity.find_by(id: id)
		when 'units'
			obj =Unit.find_by(id: id)
		when 'invoiceable_fees'
			obj =InvoiceableFee.find_by(id: id)
		when 'unit_type', 'unit_type_id'
			obj =UnitType.find_by(id: id)
		when 'discount_plan_discounts'
			obj =DiscountPlanDiscount.find_by(id: id)
		when 'discount_plan_controls'
			obj =DiscountPlanControl.find_by(id: id)
		when 'client_applications'
			obj =ClientApplication.find_by(id: id)
		when 'discount_plans', 'discount_plan_ids'
			obj =DiscountPlan.find_by(id: id)
		when 'unit_groups', 'unit_group_id'
			obj =UnitGroup.find_by(id: id)
		when 'facility', 'facility_id','facility_ids'
			obj =Facility.find_by(id: id)
		when 'channel', 'channel_id'
			obj =Channel.find_by(id: id)
		when 'scheduled_move_out', 'scheduled_move_out_ids'
			obj =ScheduledMoveOut.find_by(id: id)
		when 'tenant_account_kind', 'tenant_account_kind_id'
			obj =TenantAccountKind.find_by(id: id)
		when 'api_association', 'api_association_ids'
			obj =ApiAssociation.find_by(id: id)
		when 'current_tenant', 'current_tenant_id'
			obj = CurrentTenant.find_by(id: id)
		when 'current_ledger', 'current_ledger_id'
			obj = CurrentLedger.find_by(id: id)
		when 'always_discount_plan_discount_id', 'always_discount_plan_discount'
			obj = AlwaysDiscountPlanDiscount.find_by(id: id)
		else
			puts clss
		end
		return obj
	end


	def self.newInstance(type,values)
		puts "######"
		puts type
		puts values
		puts "######"
		case type
		when 'channel_rate', 'channel_rate_id', 'channel_rate_ids'
			ChannelRate.new(values)
		when 'unit_amenities', 'unit_amemity_ids'
			UnitAmenity.new(values)
		when 'units'
			Unit.new(values)
		when 'invoiceable_fees'
			InvoiceableFee.new(values)
		when 'unit_type', 'unit_type_id'
			UnitType.new(values)
		when 'discount_plan_discounts'
			DiscountPlanDiscount.new(values)
		when 'discount_plan_controls'
			DiscountPlanControl.new(values)
		when 'client_applications'
			ClientApplication.new(values)
		when 'discount_plans', 'discount_plan_ids'
			DiscountPlan.new(values)
		when 'unit_groups', 'unit_group_id'
			UnitGroup.new(values)
		when 'facility', 'facility_id','facility_ids'
			Facility.new(values)
		when 'channel', 'channel_id'
			Channel.new(values)
		when 'scheduled_move_out', 'scheduled_move_out_ids'
			ScheduledMoveOut.new(values)
		when 'tenant_account_kind', 'tenant_account_kind_id'
			TenantAccountKind.new(values)
		when 'current_tenant', 'current_tenant_id'
			CurrentTenant.new(values)
		when 'current_ledger', 'current_ledger_id'
			CurrentLedger.new(values)
		when 'api_association', 'api_association_ids'
			ApiAssociation.new(values)
		when 'always_discount_plan_discount_id', 'always_discount_plan_discount'
			AlwaysDiscountPlanDiscount.new(values)

		else
			puts type
			raise "table is not on DB instance"
		end
	end

	
	def self.getNames()
		plurals = []
		singles = []

		ActiveRecord::Base.connection.tables.map do |model|
  		singles.push( model.singularize)
  		plurals.push(model)
		end
		return singles, plurals
	end

end

		

=end



