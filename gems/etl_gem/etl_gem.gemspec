
Gem::Specification.new do |s|
  s.name = %q{etl_gem}
  s.version = "0.0.1"
  s.licenses  = ['MIT']
  s.date = %q{2019-05-28}
  s.summary = %q{ETL gem that gets and parses data from a json to load it onto the db}
  s.files = [
    "lib/etl_gem.rb"
  ]
  s.require_paths = ["lib"]
  s.authors = ["Eduardo Rodriguez"]
end