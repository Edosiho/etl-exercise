class UnitGroup < ApplicationRecord
	self.primary_key = "id"
	has_many :scheduled_move_out
	has_many :discount_plan
	has_many :invoiceable_fee
	has_many :unit_amenities
	has_many :unit_group_channel_rate
	has_many :channel_rate, :through => :unit_group_channel_rate
	has_many :unit_type
end
