class ChannelRate < ApplicationRecord
	self.primary_key = "id"
	has_many :facility
	has_many :channel
	has_many :unit_group_channel_rate
	has_many :unit_group, :through => :unit_group_channel_rate
end
