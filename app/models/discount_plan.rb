class DiscountPlan < ApplicationRecord
	self.primary_key = "id"
	has_many :tenant_account_kind
	has_many :api_association
	has_many :discount_plans_connect
	has_many :discount_plan_control, :through => :discount_plans_connect
	has_many :client_application
	has_many :discount_plan_discount
	has_many :facility
	has_many :always_discount_plan_discount


end
