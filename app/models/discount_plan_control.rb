class DiscountPlanControl < ApplicationRecord
	self.primary_key = "id"
	has_many :unit_amenities
	has_many :unit_type
	has_many :discount_plans_connect
	has_many :discount_plan, :through => :discount_plans_connect
end
