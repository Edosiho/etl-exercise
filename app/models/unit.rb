class Unit < ApplicationRecord
	self.primary_key = "id"
	has_many :unit_amenities
	has_many :channel_rate
	has_many :unit_group
	has_many :current_ledger
	has_many :current_tenant
end
