require "json"
require "pp"

class Etl
	def self.extract(jsonfile)
			file = File.read(jsonfile)
			file = JSON.parse(file)
			file.reject! {|k| k == "meta"}
			return file
	end
end
#unit_groups.json
#DCBYVBGDGTKTAKJWYSJJ_units.json
file = Etl.extract("DCBYVBGDGTKTAKJWYSJJ_units.json")

@structured = {}
=begin
file["units"].each do |data|
	data.each do |vals|
		print "#{vals[0].class} : #{vals[0]} - "
		puts "#{vals[1].class} : #{vals[1]}  "
		if(!vals[1].is_a?(Array) and !vals[1].is_a?(Hash))
			@structured[vals[0]] = vals[1]
		end
	end
end


@structured.each do |k,v|
	puts " "
	puts "#{k} \n" 
	
		v[0].each do |y,z|
			print "#{y}:#{typeParse(z.class)}, "
		end
end

=end

#Transform
def addToArray(val,arr)
	val.each do |data|
		if(!data.is_a?(String))
			hsh = {}
			data.each do |k,v|
					if(!v.is_a?(Array) and !v.is_a?(Hash))
						hsh[k] =v
					else
						if(v.is_a?(Hash))
							hsh[k] = v["id"]
						elsif(v.is_a?(Array))
							iarr = []
							v.each do |inner|
								if(!inner["id"].nil?)
									iarr.push(inner["id"])
								else
									iarr.push(inner)
								end

							end
						hsh[k] = iarr
					end
						if(@structured[k].nil?)
							@structured[k] = dumpV(v,[])
						else
							@structured[k] = dumpV(v,@structured[k])
						end
				end
			end
				arr.push(hsh)
		end
	end

end

def dumpV(val,arr)
	if(val.is_a?(Array))
		addToArray(val,arr)
	elsif(val.is_a?(Hash))
		arr.push(cleanHashes(val))
		parseHash(val)
	end
	return arr
end

def parseHash(hashp)

	hashp.each do |k,v|
		if(v.is_a?(Array) or v.is_a?(Hash))
			if(@structured[k].nil?)
				arr = []
				@structured[k] = dumpV(v,arr)
			else
				@structured[k] = dumpV(v,@structured[k])
			end
		end
	end
end

def cleanHashes(hsh)
	new_hsh = {}
	hsh.each do |k,v|
		if(!v.is_a?(Hash))
			new_hsh[k] = v
		end
	end
	return new_hsh
end



def deleteDup(hsh)
	hsh.each do |k, v|
		arr = []
		indx = []
		for i in 0..v.length-1 do
			tmp =  v[i]["id"]
			if(!arr.include?(tmp))
				arr.push(tmp)
			else
				indx.push(i)
			end
		end
		indx = indx.sort_by { |number| -number }
		deleteIndx(v,indx)
	end
end

def deleteIndx(arr,indx)
	for i in 0..indx.size-1 do
		arr.delete_at(indx[i])
	end
end

def trimHash(hsh)
	hsh.each do |k,v|
		if(v.size == 0)
			hsh.reject! {|val| val == k}
		end
	end
end
parseHash(file)
deleteDup(@structured)
trimHash(@structured)

def typeParse(val)
	val = val.to_s.downcase 
	if(val == "falseclass" or val == "trueclass")
		val = "boolean"
	elsif(val == "fixnum")
		val = "integer"
	else
		val = val
	end
end

pp @structured["channel_rate"][0]
