class ClientApplicationsController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def show
     @client_application = ClientApplication.find(params[:id])
  end

  def index
     @client_applications = ClientApplication.all
  end
end
