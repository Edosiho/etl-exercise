class UnitsController < ApplicationController
  def index
  	@units = Unit.all
  end

  def show
  	@unit = Unit.find(params[:id])
  end

  def new
  end

  def edit
  end
end
