class UnitTypesController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def show
    @unit_type = UnitType.find(params[:id])
  end

  def index
    @unit_types = UnitType.all
    
  end
end
