class UnitGroupsController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def show
    @unit_group = UnitGroup.find(params[:id])
  end

  def index
    @unit_groups = UnitGroup.all
  end
end
