class DiscountPlanDiscountsController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def show
    @discount_plan_discount = DiscountPlanDiscount.find(params[:id])
  end

  def index
    @Discount_plan_discounts = DiscountPlanDiscount.all

  end
end
