class ChannelRatesController < ApplicationController
  def index
  	@channel_rates = ChannelRate.all
  end

  def show
  	@channel_rate = ChannelRate.find(params[:id])
  end

  def new
  end

  def edit
  end
end
