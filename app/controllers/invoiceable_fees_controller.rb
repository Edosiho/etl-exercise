class InvoiceableFeesController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def show
    @invoiceable_fee = InvoiceableFee.find(params[:id])
  end

  def index
    @invoiceable_fees = InvoiceableFee.all
  end
end
